# Endgame HTML requests parser

## DESCRIPTION:

For fast and convenient output of data from API. You can also enter, delete, or modify data in an API that is accessible.

## PREREQUISITES:

The following programs and add-ons are required for use.

Installed Python 3 (on UNIX systems and macOS is installed by default):
To install in WINDOWS:
```python
https://www.python.org/downloads/windows/ - the official site where you can download.
```
For UNIX systems and macOS: Open a terminal and enter python3. $ python3 Most likely, you will be kindly greeted by python 3:

Python 3.6.9 (default, Jan 26 2021, 15:33:00) [GCC 8.4.0] on linux Type "help", "copyright", "credits" or "license" for more information.

If so, you can be congratulated: you already have python 3. Otherwise you need to install the * python3 * package:
```python
sudo apt-get install python3
```

## Tkinter:
Installation
```python
pip install tkinter
```

## YAML extension:

```python
$ pip install pyyaml
```

# Usage:

```python
python3 endgame.py -g or --gui - Launches the program in a separate window
```

Program operation through the console:

```python
python3 endgame.py -h or --help - displays a list of all arguments and basic commands for working with the program.

python3 endgame.py [arguments] - get data on given arguments
```

optional arguments:

-h or --help show this help message and exit -g or --gui Activate GUI mode -hist or --history {show, clear} - Show 10 last requests or clear all -a or --auth username password - Set username and password -l {debug, info, warning}, --log {debug, info, warning} - Set logging level
-m or --method {GET, POST, PUT, PATCH, DELETE} - Set request method -e or --endpoint ENDPOINT - Set URL of request -p or --params PARAMS [PARAMS ...] - Set params of request -head or --headers HEADERS [HEADERS ...] - Set headers of request -b or --body BODY [BODY ...]Set body of request -tree or --tree - Set Tree view mode -table or --table - Set Table view mode (only easy cases) -r or--raw - Set Raw view mode -pretty or --pretty - Set Pretty view mode -y or --yaml - Set Yaml view mode

Authors: Oleksii Buhaiov - obuhaiov@student.ucode.world Myroslava Tararuieva - mtararuiev@student.ucode.world Yura Vel - yvel@student.ucode.world Ivan Ivanych - iivanych@student.ucode.world
