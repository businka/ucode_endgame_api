import requests
import json
import re

import simplejson


def request_mehtod(url, data):
    mehtod = data.get('Method')
    params_dict = data.get('Params')
    body_dict = data.get('Body')
    headers_dict = data.get('Headers')
    username = data.get('Auth')['username']
    password = data.get('Auth')['password']


    par = re.compile(
        r'^(?:http|ftp)s?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    if isinstance(url,str) and re.match(par, url):
        status = 0
        time = 0
        try:
            if mehtod == 'GET':
                res  = requests.get(url, params=(params_dict or body_dict), auth=(username, password), headers=headers_dict)
                time = round(res.elapsed.total_seconds(), 2)
                status = res.status_code

                data.update({'Status': status})
                data.update({'Response': json.dumps(res.json(), indent=4)})
                data.update({'Time': time})
                data.update({'Error': ''})
                return data
            elif mehtod == 'POST':
                res = requests.post(url, auth=(username, password), data=(params_dict or body_dict))
                time = round(res.elapsed.total_seconds(), 2)
                status = res.status_code

                data.update({'Status': status})
                data.update({'Response': json.dumps(res.json(), indent=4)})
                data.update({'Time': time})
                data.update({'Error': ''})
                return data
            elif mehtod == 'PUT':
                res = requests.put(url, data=(params_dict or body_dict))
                time = round(res.elapsed.total_seconds(), 2)
                status = res.status_code

                data.update({'Status': status})
                data.update({'Response': json.dumps(res.json(), indent=4)})
                data.update({'Time': time})
                data.update({'Error': ''})
                return data
            elif mehtod == 'PATCH':
                res = requests.patch(url, data=(params_dict or body_dict))
                time = round(res.elapsed.total_seconds(), 2)
                status = res.status_code

                data.update({'Status': status})
                data.update({'Response': json.dumps(res.json(), indent=4)})
                data.update({'Time': time})
                data.update({'Error': ''})
                return data
            elif mehtod == 'DELETE':
                res = requests.delete(url)
                time = round(res.elapsed.total_seconds(), 2)
                status = res.status_code

                data.update({'Status': status})
                #data.update({'Response': json.dumps(res.json(), indent=4)})
                data.update({'Time': time})
                data.update({'Error': ''})
                return data
        except simplejson.errors.JSONDecodeError as e:
            data.update({'Error': e})
            return data
        except requests.exceptions.RequestException as e:
            data.update({'Error': e})
            return data
    else:
        data.update({'Error': 'Re-evaluate the correctness of the URL input'})
        return data
